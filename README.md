# instagram-filters

Presets of Instagram-like filters. Simply use them by passing your ImageData object to one of the preset.

# Install

```bash
yarn add instagram-filters
# or
npm install instagram-filters
```

# Example

## On existing canvas

```js
import { clarendon, applyPresetOnCanvas } from 'instagram-filters';

const canvas = document.querySelector('#myCanvas');
applyPresetOnCanvas(canvas, clarendon());
```

## On existing image

```js
import { clarendon, applyPresetOnImage } from 'instagram-filters';

const image = document.querySelector('#myImage');
// Function `applyPresetOnImage` is returning a Blob
const blob = applyPresetOnImage(image, clarendon());

image.src = window.URL.createObjectURL(blob);
```

## From URL

```js
import { clarendon, applyPresetOnImageURL } from 'instagram-filters';
import myImage from './image.jpg';

// Function `applyPresetOnImage` is returning a Blob
const blob = applyPresetOnImage(myImage, clarendon());

const image = document.querySelector('#myImage');
image.src = window.URL.createObjectURL(blob);
```

## Using preset of filters manually

```js
import { clarendon } from 'instagram-filters';

const canvas = document.querySelector('#myCanvas');
const context = canvas.getContext('2d');
// Retrieve canvas pixels
const pixels = context.getImageData(0, 0, canvas.width, canvas.height);
// Transform canvas pixels with the "clarendon" filter
const filteredPixels = clarendon()(pixels);

// Replace pixels on the canvas
context.putImageData(filteredPixels, 0, 0);
```

# Available presets

- Clarendon
- Gingham
- Moon
- Lark
- Reyes
- Juno
- Slumber
- Crema
- Ludwig
- Aden
- Perpetua
- Amaro
- Mayfair
- Rise
- Hudson
- Valencia
- X-Pro II
- Sierra
- Willow
- Lo-Fi
- Inkwell
- Hefe
- Nashville
- Stinson
- Vesper
- Earlybird
- Brannan
- Sutro
- Toaster
- Walden
- 1977
- Kelvin
- Maven
- Ginza
- Skyline
- Dogpatch
- Brooklyn
- Helena
- Ashby
- Charmes

## Mapping

A mapping of `filter name` -> `filter function` is exported as `presetsMapping`:

```js
import { presetsMapping } from 'instagram-filters';
```

# Raw filters

You can also use the raw filters directly like this:

```js
import { sepia } from 'instagram-filters/lib/filters';

...

// Apply only sepia filter with a value of 0.5
const filteredPixels = sepia(0.5)(pixels);

...
```

## Available filters

- grayscale
- sepia
- brightness
- saturation
- contrast
- rgbAdjust
- colorFilter

# Credits

Filters data are coming from [filterous-2](https://github.com/girliemac/filterous-2).
