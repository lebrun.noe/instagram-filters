import {
    grayscale,
    sepia,
    brightness,
    saturation,
    contrast,
    colorFilter,
    rgbAdjust,
} from './filters';
import { pipe } from './utils/pipe';

export type Preset = (pixels: ImageData) => ImageData;

/** Clarendon: adds light to lighter areas and dark to darker areas */
export function clarendon() {
    return pipe(
        brightness(0.1),
        contrast(0.1),
        saturation(0.15),
    );
}

/** Gingham: Vintage-inspired, taking some color out */
export function gingham() {
    return pipe(
        sepia(0.04),
        contrast(-0.15),
    );
}

/** Moon: B/W, increase brightness and decrease contrast */
export function moon() {
    return pipe(
        grayscale(),
        contrast(-0.04),
    );
}

/** Lark: Brightens and intensifies colours but not red hues */
export function lark() {
    return pipe(
        brightness(0.08),
        rgbAdjust([1, 1.03, 1.05]),
    );
}

/** Reyes: a new vintage filter, gives your photos a “dusty” look */
export function reyes() {
    return pipe(
        sepia(0.4),
        brightness(0.13),
        contrast(-0.05),
    );
}

/** Juno: Brightens colors, and intensifies red and yellow hues */
export function juno() {
    return pipe(
        rgbAdjust([1.01, 1.04, 1]),
        saturation(0.3),
    );
}

/** Slumber: Desaturates the image as well as adds haze for a retro, dreamy look – with an emphasis on blacks and blues */
export function slumber() {
    return pipe(
        brightness(0.1),
        saturation(-0.5),
    );
}

/** Crema: Adds a creamy look that both warms and cools the image */
export function crema() {
    return pipe(rgbAdjust([1.04, 1, 1.02]));
}

/** Ludwig: A slight hint of desaturation that also enhances light */
export function ludwig() {
    return pipe(
        brightness(0.05),
        saturation(-0.03),
    );
}

/** Aden: This filter gives a blue/pink natural look */
export function aden() {
    return pipe(
        colorFilter([228, 130, 225, 0.13]),
        saturation(-0.2),
    );
}

/** Perpetua: Adding a pastel look, this filter is ideal for portraits */
export function perpetua() {
    return pipe(rgbAdjust([1.05, 1.1, 1]));
}

/** Amaro: Adds light to an image, with the focus on the centre */
export function amaro() {
    return pipe(
        saturation(0.3),
        brightness(0.15),
    );
}

/** Mayfair: Applies a warm pink tone, subtle vignetting to brighten the photograph center and a thin black border */
export function mayfair() {
    return pipe(
        colorFilter([230, 115, 105, 0.05]),
        saturation(0.15),
    );
}

/** Rise: Adds a "glow" to the image, with softer lighting of the subject */
export function rise() {
    return pipe(
        colorFilter([255, 170, 0, 0.1]),
        brightness(0.09),
        saturation(0.1),
    );
}

/** Hudson: Creates an "icy" illusion with heightened shadows, cool tint and dodged center */
export function hudson() {
    return pipe(
        rgbAdjust([1, 1, 1.25]),
        contrast(0.1),
        brightness(0.15),
    );
}

/** Valencia: Fades the image by increasing exposure and warming the colors, to give it an antique feel */
export function valencia() {
    return pipe(
        colorFilter([255, 225, 80, 0.08]),
        saturation(0.1),
        contrast(0.05),
    );
}

/** X-Pro II: Increases color vibrance with a golden tint, high contrast and slight vignette added to the edges */
export function xpro2() {
    return pipe(
        colorFilter([255, 255, 0, 0.07]),
        saturation(0.2),
        contrast(0.15),
    );
}

/** Sierra: Gives a faded, softer look */
export function sierra() {
    return pipe(
        contrast(-0.15),
        saturation(0.1),
    );
}

/** Willow: A monochromatic filter with subtle purple tones and a translucent white border */
export function willow() {
    return pipe(
        grayscale(),
        colorFilter([100, 28, 210, 0.03]),
        brightness(0.1),
    );
}

/** Lo-Fi: Enriches color and adds strong shadows through the use of saturation and "warming" the temperature */
export function lofi() {
    return pipe(
        contrast(0.15),
        saturation(0.2),
    );
}

/** Inkwell: Direct shift to black and white */
export function inkwell() {
    return pipe(grayscale());
}

/** Hefe: Hight contrast and saturation, with a similar effect to Lo-Fi but not quite as dramatic */
export function hefe() {
    return pipe(
        contrast(0.1),
        saturation(0.15),
    );
}

/** Nashville: Warms the temperature, lowers contrast and increases exposure to give a light "pink" tint – making it feel "nostalgic" */
export function nashville() {
    return pipe(
        colorFilter([220, 115, 188, 0.12]),
        contrast(-0.05),
    );
}

/** Stinson: washing out the colors ever so slightly */
export function stinson() {
    return pipe(
        brightness(0.1),
        sepia(0.3),
    );
}

/** Vesper: adds a yellow tint that */
export function vesper() {
    return pipe(
        colorFilter([255, 225, 0, 0.05]),
        brightness(0.06),
        contrast(0.06),
    );
}

/** Earlybird: Gives an older look with a sepia tint and warm temperature */
export function earlybird() {
    return pipe(colorFilter([255, 165, 40, 0.2]));
}

/** Brannan: Increases contrast and exposure and adds a metallic tint */
export function brannan() {
    return pipe(
        contrast(0.2),
        colorFilter([140, 10, 185, 0.1]),
    );
}

/** Sutro: Burns photo edges, increases highlights and shadows dramatically with a focus on purple and brown colors */
export function sutro() {
    return pipe(
        brightness(-0.1),
        saturation(-0.1),
    );
}

/** Toaster: Ages the image by "burning" the centre and adds a dramatic vignette */
export function toaster() {
    return pipe(
        sepia(0.1),
        colorFilter([255, 145, 0, 0.2]),
    );
}

/** Walden: Increases exposure and adds a yellow tint */
export function walden() {
    return pipe(
        brightness(0.1),
        colorFilter([255, 255, 0, 0.2]),
    );
}

/** 1977: The increased exposure with a red tint gives the photograph a rosy, brighter, faded look. */
export function year1977() {
    return pipe(
        colorFilter([255, 25, 0, 0.15]),
        brightness(0.1),
    );
}

/** Kelvin: Increases saturation and temperature to give it a radiant "glow" */
export function kelvin() {
    return pipe(
        colorFilter([255, 140, 0, 0.1]),
        rgbAdjust([1.15, 1.05, 1]),
    );
}

/** Maven: darkens images, increases shadows, and adds a slightly yellow tint overal */
export function maven() {
    return pipe(
        colorFilter([225, 240, 0, 0.1]),
        saturation(0.25),
        contrast(0.05),
    );
}

/** Ginza: brightens and adds a warm glow */
export function ginza() {
    return pipe(
        sepia(0.06),
        brightness(0.1),
    );
}

/** Skyline: brightens to the image pop */
export function skyline() {
    return pipe(
        saturation(0.35),
        brightness(0.1),
    );
}

/** Dogpatch: increases the contrast, while washing out the lighter colors */
export function dogpatch() {
    return pipe(
        contrast(0.15),
        brightness(0.1),
    );
}

/** Brooklyn */
export function brooklyn() {
    return pipe(
        colorFilter([25, 240, 252, 0.05]),
        sepia(0.3),
    );
}

/** Helena: adds an orange and teal vibe */
export function helena() {
    return pipe(
        colorFilter([208, 208, 86, 0.2]),
        contrast(0.15),
    );
}

/** Ashby: gives images a great golden glow and a subtle vintage feel */
export function ashby() {
    return pipe(
        colorFilter([255, 160, 25, 0.1]),
        brightness(0.1),
    );
}

/** Charmes: a high contrast filter, warming up colors in your image with a red tint */
export function charmes() {
    return pipe(
        colorFilter([255, 50, 80, 0.12]),
        contrast(0.05),
    );
}

export interface IPresetMapping {
    [name: string]: () => Preset;
}

export const presetsMapping: IPresetMapping = {
    Clarendon: clarendon,
    Gingham: gingham,
    Moon: moon,
    Lark: lark,
    Reyes: reyes,
    Juno: juno,
    Slumber: slumber,
    Crema: crema,
    Ludwig: ludwig,
    Aden: aden,
    Perpetua: perpetua,
    Amaro: amaro,
    Mayfair: mayfair,
    Rise: rise,
    Hudson: hudson,
    Valencia: valencia,
    ['X-Pro II']: xpro2,
    Sierra: sierra,
    Willow: willow,
    ['Lo-Fi']: lofi,
    Inkwell: inkwell,
    Hefe: hefe,
    Nashville: nashville,
    Stinson: stinson,
    Vesper: vesper,
    Earlybird: earlybird,
    Brannan: brannan,
    Sutro: sutro,
    Toaster: toaster,
    Walden: walden,
    ['1977']: year1977,
    Kelvin: kelvin,
    Maven: maven,
    Ginza: ginza,
    Skyline: skyline,
    Dogpatch: dogpatch,
    Brooklyn: brooklyn,
    Helena: helena,
    Ashby: ashby,
    Charmes: charmes,
};
