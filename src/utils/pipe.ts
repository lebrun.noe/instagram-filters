export function pipe<R>(
    firstFn: (param: R) => R,
    ...fns: Array<(param: R) => R>
) {
    return fns.reduce(
        (prevFn, nextFn) => value => nextFn(prevFn(value)),
        firstFn,
    );
}
