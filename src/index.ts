import { pipe } from './utils/pipe';
import { InstagramFiltersError } from './utils/InstagramFiltersError';
import { Preset } from './presets';

export function applyPresetOnCanvas(
    canvas: HTMLCanvasElement,
    ...presets: Preset[]
) {
    if (!canvas) {
        throw new InstagramFiltersError('canvas argument must be defined');
    }

    if (!presets || !presets.length) {
        throw new InstagramFiltersError('At least one preset must be passed');
    }

    const [firstPreset, ...nextPresets] = presets;
    const context = canvas.getContext('2d');

    if (!context) {
        throw new InstagramFiltersError(
            'Unable to retrieve context from the canvas',
        );
    }

    const pixels = context.getImageData(0, 0, canvas.width, canvas.height);
    const filteredPixels = pipe(
        firstPreset,
        ...nextPresets,
    )(pixels);

    context.putImageData(filteredPixels, 0, 0);
}

export async function applyPresetOnImage(
    image: HTMLImageElement,
    ...presets: Preset[]
) {
    if (!image) {
        throw new InstagramFiltersError('image argument must be defined');
    }

    const canvas = document.createElement('canvas');
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;

    const context = canvas.getContext('2d');

    if (!context) {
        throw new InstagramFiltersError(
            'Unable to retrieve context from the canvas',
        );
    }

    context.drawImage(image, 0, 0, canvas.width, canvas.height);
    applyPresetOnCanvas(canvas, ...presets);

    const blob = await new Promise<Blob | null>(resolve =>
        canvas.toBlob(resolve),
    );
    canvas.remove();

    return blob;
}

export async function applyPresetOnImageURL(url: string, ...presets: Preset[]) {
    const image = await new Promise<HTMLImageElement>((resolve, reject) => {
        const imageElement = new Image();
        imageElement.addEventListener('load', () => resolve(imageElement));
        imageElement.addEventListener('error', reject);
        imageElement.src = url;
    });

    return applyPresetOnImage(image, ...presets);
}

export * from './presets';
